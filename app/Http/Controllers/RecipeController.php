<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\RecipeResource;
use Google_Client;
use Google_Service_YouTube;

class RecipeController extends Controller
{
    public function index() {
        return RecipeResource::collection(Recipe::inRandomOrder()->limit(30)->get());
    }

    public function recipe($recipeId) {
        $recipe = Recipe::where('slug', $recipeId)->first();
        return new RecipeResource($recipe);
    }

    public function search($item)
    {
        $response = Http::get('https://api.edamam.com/search', [
            'q' => $item,
            'app_id' => 'ba51e467',
            'app_key' => '890376e9f860da1aa63f1513f87b3005'
        ]);

        $data = $response->json();
        $recipes = [];

        if ($data['hits']) {
            foreach ($data['hits'] as $item) {
                $recipe = $item['recipe'];
                $recipeId = str_replace('http://www.edamam.com/ontologies/edamam.owl#', '', $recipe['uri']);
                
                $getRecipe = Recipe::where('slug', $recipeId)->first();

                if (!$getRecipe) {
                    $getRecipe = Recipe::create([
                        'slug' => $recipeId,
                        'data' => json_encode($recipe)
                    ]);
                }

                array_push($recipes, $getRecipe);
            }
        }

        return RecipeResource::collection($recipes);
    }

    public function youtube($item)
    {
        $developerKey = 'AIzaSyC5dhBcp0t-Bd12NvV7rgCL0Sux7tm8jqo';
        $client = new Google_Client();
        $client->setDeveloperKey($developerKey);

        $youtube = new Google_Service_YouTube($client);
        $searchResponse = $youtube->search->listSearch('id', array(
            'q' => $item,
            'maxResults' => 9,
        ));

        return response()->json($searchResponse);
    }
}
