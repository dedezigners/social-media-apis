export default {
    setUserAuth(state, value) {
        state.isAuth = value;
    },
    saveUserinfo(state, payload) {
        state.user = payload;
    },
    saveRecipes(state, payload) {
        state.recipes = payload;
    },
    updateRecipes(state, payload) {
        payload.forEach(recipe => {
            state.recipes.push(recipe);
        });
    }
};