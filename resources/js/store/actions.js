export default {
    getAuthentiactedUser({ commit }) {
        axios.get('/user-info')
        .then(res => {
            commit('saveUserinfo', res.data.data);
        });
    },
    getMostSearchRecipes({ commit }) {
        axios.get('/recipes')
        .then(res => commit('saveRecipes', res.data.data))
    }
};