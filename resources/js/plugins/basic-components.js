import Vue from 'vue';
import AppHeader from '../components/AppHeader';
import LoadSvg from '../components/widgets/LoadSvg';

Vue.component('app-header', AppHeader);
Vue.component('load-svg', LoadSvg);