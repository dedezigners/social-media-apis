<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Foodies &mdash; Find Food Recipes</title>
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />

    <!-- Template Core Style -->
    <link rel="stylesheet" href="{{ '/assets/css/bulma.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/app.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/core.css' }}">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
    <div id="app"></div>

    <div class="container">
        <div id="disqus_thread"></div>
    </div>
    
    <script>
        var disqus_config = function () {
            this.page.url = window.location.href;
            this.page.identifier = window.location.pathname;
        };

        (function() {
            // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://foodies-8.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>