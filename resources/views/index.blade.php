<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Foodies &mdash; Find Food Recipes</title>
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />

    <!-- Template Core Style -->
    <link rel="stylesheet" href="{{ '/assets/css/bulma.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/app.css' }}">
    <link rel="stylesheet" href="{{ '/assets/css/core.css' }}">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
    <div id="app"></div>

    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>