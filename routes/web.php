<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/callback/twitter', 'TwitterController@callback');
Route::get('/callback/facebook', 'FacebookController@callback');

Route::get('/', function() { return view('index'); });
Route::get('/{slug}', function() { return view('index'); });
Route::get('/item/{slug}', function() { return view('comments'); });
Route::get('/{slug}/{any}', function() { return view('index'); });
